import { combineReducers } from "redux";

import connection from "./Connection";

const rootReducer = combineReducers({
  connection,
});

export default rootReducer;
