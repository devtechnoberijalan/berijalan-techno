import React from "react";
import { Text } from "@atoms/index";
import { Colors, horizontalScale, rgbaConverter } from "@styles/index";
import { TouchableOpacity, ImageBackground, View, Image } from "react-native";

import { FlatList } from "react-native-gesture-handler";
import { cards } from "./data";

import { key } from "@utils/helper";

import CardsItem from "./CardItems";

const Projects = () => {
  return (
    <View>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Text bold fontSize={30} style={{ flex: 1 }}>
          Projects
        </Text>
        <TouchableOpacity
          style={{
            backgroundColor: Colors.WhiteSystem.Heavy,
            paddingVertical: 8,
            paddingHorizontal: 25,
            borderRadius: 90,
          }}
        >
          <Text bold fontSize={16}>
            + Add
          </Text>
        </TouchableOpacity>
      </View>
      <Text bold fontSize={16} color={Colors.TextColor.Teks50}>
        You have {cards.length} projects
      </Text>

      <FlatList
        data={cards}
        horizontal
        showsHorizontalScrollIndicator={false}
        renderItem={CardsItem}
        keyExtractor={(_, index) => key(index)}
        style={{ marginTop: 20, marginHorizontal: -16 }}
      />
    </View>
  );
};

export default Projects;
