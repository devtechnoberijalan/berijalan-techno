import React from "react";
import { View, ImageBackground, Image, TouchableOpacity } from "react-native";

import Text from "@atoms/Text";
import { Colors, horizontalScale, rgbaConverter } from "@styles/index";
import { cards } from "../data";
import { RowRight } from "@images/index";

import { key } from "@utils/helper";

const CardsItem = ({ item, index }) => {
  return (
    <ImageBackground
      source={item.card}
      resizeMode="cover"
      style={{
        width: horizontalScale(215),
        height: horizontalScale(295),
        marginLeft: index === 0 ? 16 : 0,
        marginRight: 16,
        borderRadius: 12,
        overflow: "hidden",
      }}
    >
      <View
        style={{
          flex: 1,
          padding: 22,
          backgroundColor: rgbaConverter(Colors.Black, 0.16),
        }}
      >
        <Text bold fontSize={24} color={Colors.White} underline>
          {item?.name}
        </Text>
        <Text bold fontSize={16} color={Colors.TextColor.Teks30}>
          {item?.subitem}
        </Text>

        <View style={{ marginTop: 10, flex: 1 }}>
          {item?.team
            .filter((_, index) => index < 3)
            .map((value, index) => (
              <View
                key={key(index)}
                style={{
                  borderWidth: 1,
                  borderColor: Colors.White,
                  width: horizontalScale(35),
                  height: horizontalScale(35),
                  borderRadius: 90,
                  overflow: "hidden",
                  marginTop: index !== 0 ? -10 : 0,
                }}
              >
                {item?.team.length > 3 && index === 2 ? (
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: Colors.Random(),
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Text color={Colors.White}>+{item?.team.length - 2}</Text>
                  </View>
                ) : (
                  <Image
                    source={value}
                    resizeMode="cover"
                    style={{
                      borderRadius: 90,
                      width: horizontalScale(34),
                      height: horizontalScale(34),
                    }}
                  />
                )}
              </View>
            ))}
        </View>

        <TouchableOpacity
          activeOpacity={0.7}
          style={{
            backgroundColor: Colors.RedSystem.Massive,
            marginHorizontal: -15,
            marginBottom: -15,
            borderRadius: 12,
            paddingHorizontal: 20,
            paddingVertical: 16,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Text fontSize={16} bold color={Colors.White} style={{ flex: 1 }}>
            View Project
          </Text>

          <View
            style={{
              backgroundColor: Colors.BlackSystem.Fair,
              borderRadius: 90,
              width: horizontalScale(30),
              height: horizontalScale(30),
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <RowRight
              width={horizontalScale(16)}
              height={horizontalScale(16)}
              stroke={Colors.White}
            />
          </View>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default CardsItem;
