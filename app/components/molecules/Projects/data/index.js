import images from "@images/index";
import uuid from "react-native-uuid";

const cards = [
  {
    name: "Berijalan",
    subitem: "Berijalan Project",
    card: images.CARD_BERIJALAN,
    team: [images.PERSON1, images.PERSON3, images.PERSON9],
    navigate: "",
  },
  {
    name: "Setir Kanan",
    subitem: "Setir Kanan Sync B2B",
    card: images.CARD_SETIRKANAN,
    team: [
      images.PERSON5,
      images.PERSON9,
      images.PERSON4,
      images.PERSON6,
      images.PERSON2,
      images.PERSON8,
    ],
    navigate: "",
  },
  {
    name: "Moxa",
    subitem: "Moxa 2023",
    card: images.CARD_MOXA,
    team: [
      images.PERSON8,
      images.PERSON10,
      images.PERSON3,
      images.PERSON7,
      images.PERSON1,
      images.PERSON4,
      images.PERSON2,
      images.PERSON9,
    ],
    navigate: "",
  },
];

export { cards };
