import Projects from "./Projects";
import TimeManagements from "./TimeManagements";

//Profile
import ImageProfile from "./Profile/ImageProfile";
import AppInfo from "./Profile/AppInfo";
import PersonalInfo from "./Profile/PersonalInfo";
import TeamProject from "./Profile/TeamProject";

export {
  Projects,
  TimeManagements,
  ImageProfile,
  AppInfo,
  PersonalInfo,
  TeamProject,
};
