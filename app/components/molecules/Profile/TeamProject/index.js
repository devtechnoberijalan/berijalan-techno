import React from "react";
import { View, TouchableOpacity, Image } from "react-native";
import styles from "../styles";
import { Text } from "@atoms/index";
import { Colors, Shadow, horizontalScale } from "@styles/index";
import { dataTeam } from "../data";
import { key } from "@utils/helper";

const TeamProject = () => {
  return (
    <View style={[styles.personalnfo, Shadow]}>
      <TouchableOpacity style={styles.pressPersonal}>
        <View style={{ flex: 1 }}>
          <Text bold style={styles.textValue}>
            Team
          </Text>
          <Text bold color={Colors.TextColor.Teks50}>
            {dataTeam.team}
          </Text>
        </View>

        <View style={{ flexDirection: "row" }}>
          {dataTeam.member
            .filter((_, index) => index < 4)
            .map((value, index) => (
              <View
                key={key(index)}
                style={{
                  borderWidth: 1,
                  borderColor: Colors.White,
                  width: horizontalScale(35),
                  height: horizontalScale(35),
                  borderRadius: 90,
                  overflow: "hidden",
                  marginLeft: index !== 0 ? -14 : 0,
                }}
              >
                {dataTeam.member.length > 3 && index === 3 ? (
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: Colors.RedSystem.Massive,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Text color={Colors.White}>
                      +{dataTeam.member.length - 3}
                    </Text>
                  </View>
                ) : (
                  <Image
                    source={value}
                    resizeMode="cover"
                    style={{
                      borderRadius: 90,
                      width: horizontalScale(34),
                      height: horizontalScale(34),
                    }}
                  />
                )}
              </View>
            ))}
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default TeamProject;
