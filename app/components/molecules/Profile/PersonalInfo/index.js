import { Shadow, verticalScale } from "@styles/index";
import React from "react";
import { View, TouchableOpacity } from "react-native";
import styles from "../styles";
import { Text } from "@atoms/index";
import { personInf } from "../data";
import { key } from "@utils/helper";

const PersonalInfo = () => {
  return (
    <View style={[styles.personalnfo, Shadow]}>
      {personInf.map((value, index) => (
        <View key={key(index)}>
          <TouchableOpacity style={styles.pressPersonal}>
            <Text bold style={styles.textValue}>
              {value.key}
            </Text>
            <Text bold style={styles.textInfo}>
              {value.value}
            </Text>
          </TouchableOpacity>
          {index !== personInf.length - 1 && <View style={styles.line} />}
        </View>
      ))}
    </View>
  );
};

export default PersonalInfo;
