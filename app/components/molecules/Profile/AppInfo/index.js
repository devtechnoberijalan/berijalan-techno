import { Colors, Shadow, horizontalScale, verticalScale } from "@styles/index";
import React from "react";
import { View, TouchableOpacity } from "react-native";
import styles from "../styles";
import { Text } from "@atoms/index";
import { appInf } from "../data";
import { key } from "@utils/helper";
import { ChevronRight } from "@images/index";

const AppInfo = () => {
  return (
    <View style={[styles.personalnfo, Shadow]}>
      {appInf.map((value, index) => (
        <View key={key(index)}>
          <TouchableOpacity style={styles.pressPersonal}>
            {value.icon}
            <Text bold style={styles.textApp}>
              {value.key}
            </Text>
            <ChevronRight
              width={horizontalScale(14)}
              height={horizontalScale(14)}
              stroke={Colors.TextColor.Teks}
            />
          </TouchableOpacity>
          {index !== appInf.length - 1 && <View style={styles.line} />}
        </View>
      ))}
    </View>
  );
};

export default AppInfo;
