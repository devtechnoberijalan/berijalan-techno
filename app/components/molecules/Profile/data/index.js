import images, {
  AboutIcon,
  HelpIcon,
  LogoutIcon,
  SecurityIcon,
} from "@images/index";
import { Colors, horizontalScale } from "@styles/index";
import { View } from "react-native";
import styles from "../styles";

const personInf = [
  { key: "ID", value: "A20134" },
  { key: "Email", value: "siapanamanyayah@gmail.com" },
  { key: "Date of Birth", value: "3 May 1998" },
  { key: "Gander", value: "Male" },
];

const dataTeam = {
  team: "React Native",
  member: [
    images.PERSON6,
    images.PERSON10,
    images.PERSON3,
    images.PERSON7,
    images.PERSON1,
    images.PERSON4,
    images.PERSON5,
    images.PERSON9,
    images.PERSON8,
  ],
};

const appInf = [
  {
    key: "Privacy and Security",
    icon: (
      <View style={[styles.iconInfo, { backgroundColor: Colors.Grey.Fair }]}>
        <SecurityIcon
          width={horizontalScale(14)}
          height={horizontalScale(14)}
          fill={Colors.BlackSystem.Fair}
        />
      </View>
    ),
  },
  {
    key: "Help",
    icon: (
      <View style={[styles.iconInfo, { backgroundColor: Colors.Warning.Root }]}>
        <HelpIcon
          width={horizontalScale(14)}
          height={horizontalScale(14)}
          fill={Colors.BlackSystem.Fair}
        />
      </View>
    ),
  },
  {
    key: "About Us",
    icon: (
      <View style={[styles.iconInfo, { backgroundColor: Colors.Malibu.Soft }]}>
        <AboutIcon
          width={horizontalScale(14)}
          height={horizontalScale(14)}
          fill={Colors.BlackSystem.Fair}
        />
      </View>
    ),
  },
  {
    key: "Logout",
    icon: (
      <View
        style={[styles.iconInfo, { backgroundColor: Colors.RedSystem.Fair }]}
      >
        <LogoutIcon
          width={horizontalScale(14)}
          height={horizontalScale(14)}
          fill={Colors.BlackSystem.Fair}
        />
      </View>
    ),
  },
];

export { personInf, dataTeam, appInf };
