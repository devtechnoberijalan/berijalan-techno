import React from "react";
import { View, ImageBackground, Image, TouchableOpacity } from "react-native";
import images from "@images/index";
import { Text } from "@atoms/index";
import {
  Colors,
  Height,
  Width,
  horizontalScale,
  verticalScale,
} from "@styles/index";

const ImageProfile = () => {
  return (
    <ImageBackground
      source={images.BG_PROFILE}
      resizeMode="cover"
      style={{
        // borderWidth: 1,
        width: Width,
        height: Height * 0.6,
        alignItems: "center",
      }}
    >
      <TouchableOpacity
        activeOpacity={0.7}
        style={{
          borderWidth: 2,
          borderColor: Colors.TreePoppy.Root,
          borderRadius: 90,
          marginTop: verticalScale(85),
          overflow: "hidden",
          backgroundColor: "transparent",
        }}
      >
        <Image
          source={images.PERSON2}
          resizeMode="cover"
          style={{
            width: horizontalScale(120),
            height: horizontalScale(120),
          }}
        />
      </TouchableOpacity>

      <Text bold fontSize={20} style={{ marginTop: 16 }}>
        Ahmad Akbar Maulana
      </Text>
      <Text bold color={Colors.TextColor.Teks60}>
        React Native Developer
      </Text>
    </ImageBackground>
  );
};

export default ImageProfile;
