import { StyleSheet } from "react-native";
import {
  Colors,
  horizontalScale,
  moderateScale,
  verticalScale,
} from "@styles/index";

export default StyleSheet.create({
  container: {
    marginTop: verticalScale(45),
  },

  personalnfo: {
    borderRadius: 1,
    padding: 16,
    backgroundColor: Colors.White,
    borderRadius: 12,
    marginTop: 20,
  },
  pressPersonal: {
    flexDirection: "row",
    alignItems: "center",
  },

  textValue: {
    // borderWidth: 1,
    flex: 0.5,
  },
  textInfo: {
    // borderWidth: 1,
    flex: 1,
    textAlign: "right",
    color: Colors.TextColor.Teks50,
  },

  line: {
    borderTopWidth: 1,
    marginVertical: 14,
    borderColor: Colors.TextColor.Teks30,
  },

  textApp: {
    flex: 1,
  },

  iconInfo: {
    // borderWidth: 1,
    borderRadius: 8,
    width: horizontalScale(24),
    height: horizontalScale(24),
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
  },

  appVersion: {
    marginTop: 20,
    textAlign: "center",
  },
});
