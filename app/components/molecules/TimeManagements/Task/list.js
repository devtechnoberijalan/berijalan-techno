import React from "react";
import { View, Image } from "react-native";
import { Card, Text } from "@atoms/index";
import { Colors, horizontalScale } from "@styles/index";
import { EditIcon } from "@images/index";
import { key } from "@utils/helper";

const ListTask = ({ item, index }) => {
  return (
    <Card
      key={key(index)}
      style={{
        marginVertical: 16,
        marginHorizontal: 10,
        width: horizontalScale(180),
        height: horizontalScale(180),
      }}
    >
      <View style={{ flexDirection: "row" }}>
        <Text bold fontSize={18} style={{ flex: 1 }}>
          {item.project}
        </Text>
        <View>
          <EditIcon
            width={horizontalScale(14)}
            height={horizontalScale(14)}
            fill={Colors.TextColor.Teks60}
          />
        </View>
      </View>
      <View style={{ flexDirection: "row", flex: 1, marginTop: 5 }}>
        {item?.team
          .filter((_, index) => index < 3)
          .map((value, index) => (
            <View
              key={key(index)}
              style={{
                borderWidth: 1,
                borderColor: Colors.White,
                width: horizontalScale(25),
                height: horizontalScale(25),
                borderRadius: 90,
                overflow: "hidden",
                marginLeft: index !== 0 ? -10 : 0,
              }}
            >
              {item?.team.length > 3 && index === 2 ? (
                <View
                  style={{
                    flex: 1,
                    backgroundColor: Colors.Random(),
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Text color={Colors.White}>+{item?.team.length - 2}</Text>
                </View>
              ) : (
                <Image
                  source={value}
                  resizeMode="cover"
                  style={{
                    borderRadius: 90,
                    width: horizontalScale(24),
                    height: horizontalScale(24),
                  }}
                />
              )}
            </View>
          ))}
      </View>
      <View>
        <Text bold fontSize={18}>
          Todo
        </Text>
        <Text color={Colors.TextColor.Teks60} maxLine={2}>
          {item.todo}
        </Text>
      </View>
    </Card>
  );
};

export default ListTask;
