import { View, Image } from "react-native";
import React from "react";
import { Card, DashLines, Text } from "@atoms/index";
import { FlatList } from "react-native-gesture-handler";
import { dataTask } from "../data";
import { key } from "@utils/helper";
import { Colors, Width, horizontalScale } from "@styles/index";
import { EditIcon } from "@images/index";
import ListTask from "./list";

const Task = () => {
  return (
    <View
      style={[
        {
          // borderWidth: 1,
          marginTop: 20,
          marginHorizontal: -16,
          paddingLeft: 16,
          overflow: "hidden",
          backgroundColor: Colors.Malibu.Root,
        },
      ]}
    >
      {dataTask.task.map((item, index) => (
        <View key={key(index)} style={{ flexDirection: "row" }}>
          <View style={{ alignItems: "center" }}>
            <Text
              bold
              fontSize={16}
              style={{
                // borderWidth: 1,
                marginTop: 27,
                backgroundColor: Colors.Malibu.Root,
                paddingTop: 20,
              }}
            >
              {item.date}
            </Text>
            <DashLines
              color={Colors.TextColor.Teks50}
              dash={true}
              width={1.5}
              height={180}
              style={{
                position: "absolute",
                flexDirection: "column",
                marginTop: 16,
              }}
            />
          </View>

          <FlatList
            data={item.listTask}
            horizontal
            ListEmptyComponent={() => (
              <View
                style={{
                  width: Width * 0.75,
                  height: horizontalScale(180),
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text bold fontSize={16}>
                  Empty
                </Text>
              </View>
            )}
            showsHorizontalScrollIndicator={false}
            renderItem={ListTask}
            keyExtractor={(_, index) => key(index)}
            contentContainerStyle={{ paddingRight: item.listTask ? 30 : 0 }}
            style={{
              // borderWidth: 1,
              marginTop: 20,
              marginLeft: 10,
            }}
          />
        </View>
      ))}
    </View>
  );
};

export default Task;
