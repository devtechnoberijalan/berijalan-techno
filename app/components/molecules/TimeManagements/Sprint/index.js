import React, { useState, useRef, useCallback, useEffect } from "react";
import { Platform, View } from "react-native";
import { dataSprint } from "../data";
import { key } from "@utils/helper";
import { ScrollView } from "react-native-gesture-handler";
import { Text } from "@atoms/index";
import { Colors, horizontalScale } from "@styles/index";

const Sprint = ({ selected, srintNow }) => {
  const [scrollRef, setScrollRef] = useState(null);
  const [sprint, setSprint] = useState([]);

  const scrollHandler = () => {
    const result = sprint.find(({ title }) => title === dataSprint[selected]);
    // console.log("RESS", result);
    scrollRef?.scrollTo({
      x: result?.direction - horizontalScale(250),
      y: 0,
      animated: true,
    });
  };

  return (
    <View style={{ marginHorizontal: -16, marginTop: 10 }}>
      <ScrollView
        ref={(ref) => setScrollRef(ref)}
        horizontal
        showsHorizontalScrollIndicator={false}
        scrollEnabled={false}
      >
        <View style={{ flexDirection: "row" }}>
          {dataSprint.map((value, index) => (
            <View
              key={key(index)}
              style={{
                marginLeft: horizontalScale(index === 0 ? 250 : 0),
                marginRight: horizontalScale(
                  index === dataSprint.length - 1 ? 70 : 40
                ),
              }}
              onLayout={(event) => {
                const layout = event.nativeEvent.layout;

                sprint.push({ title: value, direction: layout.x });

                scrollHandler();
                // if (Platform.OS === "ios") {
                //   console.log("DATA IOS", sprint);
                // } else {
                //   console.log("DATA Andro", sprint);
                // }
              }}
            >
              <Text
                semiBold={index !== selected}
                bold={index === selected}
                color={
                  index === selected
                    ? Colors.TextColor.Teks
                    : Colors.TextColor.Teks60
                }
              >
                {value.title}
              </Text>
            </View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default Sprint;
