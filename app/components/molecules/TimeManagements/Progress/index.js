import React, { useCallback, useEffect } from "react";
import { View, TouchableOpacity } from "react-native";
import { DashLines, Text } from "@atoms/index";
import { Colors, horizontalScale } from "@styles/index";
import { ChevronLeft, ChevronRight } from "@images/index";
import { dataSprint } from "../data";

const Progress = ({ status, selected, plus, minus }) => {
  return (
    <View>
      <View
        style={{
          //   borderWidth: 1,
          alignItems: "center",
          position: "absolute",
          right: 0,
          marginTop: 10,
          width: horizontalScale(167),
        }}
      >
        <Text
          fontSize={24}
          color={Colors.TextColor.Teks50}
          style={{ marginTop: -13 }}
        >
          &#8226;
        </Text>
        <DashLines
          color={Colors.TextColor.Teks50}
          dash={false}
          width={1.5}
          height={20}
          style={{ marginTop: -13 }}
        />
      </View>
      <View style={{ flexDirection: "row", marginTop: 36 }}>
        <View style={{ flex: 1 }}>
          <Text bold fontSize={16}>
            {dataSprint[selected].timeline}
          </Text>
          <Text semiBold color={Colors.TextColor.Teks60}>
            Progress {dataSprint[selected].progress * 100} %
          </Text>
        </View>
        <View
          style={{
            // borderWidth: 1,
            width: horizontalScale(170),
            justifyContent: "center",
          }}
        >
          <View
            style={{
              borderWidth: 1,
              borderColor: Colors.TextColor.Teks60,
              borderRadius: 90,
              flexDirection: "row",
              alignItems: "center",
              overflow: "hidden",
              backgroundColor: "transparent",
            }}
          >
            <TouchableOpacity
              onPress={() => minus()}
              style={{
                paddingVertical: 14,
                paddingLeft: 16,
                paddingRight: 10,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <ChevronLeft
                width={horizontalScale(14)}
                height={horizontalScale(14)}
                stroke={Colors.TextColor.Teks60}
              />
            </TouchableOpacity>
            <Text style={{ flex: 1, textAlign: "center" }} fontSize={16} bold>
              {status}
            </Text>
            <TouchableOpacity
              onPress={() => plus()}
              style={{
                paddingVertical: 14,
                paddingLeft: 10,
                paddingRight: 16,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <ChevronRight
                width={horizontalScale(14)}
                height={horizontalScale(14)}
                stroke={Colors.TextColor.Teks60}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          //   borderWidth: 1,
          alignItems: "center",
          position: "absolute",
          right: 0,
          bottom: -40,
          width: horizontalScale(167),
        }}
      >
        <DashLines
          color={Colors.TextColor.Teks50}
          dash={false}
          width={1.5}
          height={40}
          style={{ marginTop: -13 }}
        />
      </View>
    </View>
  );
};

export default Progress;
