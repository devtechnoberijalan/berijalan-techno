import images from "@images/index";

const dataSprint = [
  { title: "Sprint 1", progress: 1, timeline: "Jan 9 - 20" },
  { title: "Sprint 2", progress: 1, timeline: "Jan 23 - Feb 3" },
  { title: "Sprint 3", progress: 1, timeline: "Feb 6 - 17" },
  { title: "Sprint 4", progress: 1, timeline: "Feb 20 - Mar 3" },
  { title: "Sprint 5", progress: 1, timeline: "Mar 6 - 17" },
  { title: "Sprint 6", progress: 1, timeline: "Mar 20 - 31" },
  { title: "Sprint 7", progress: 0.4, timeline: "Apr 3 - 14" },
  { title: "Sprint 8", progress: 0, timeline: "Apr 17 - 28" },
  { title: "Sprint 9", progress: 0, timeline: "May 1 - 12" },
  { title: "Sprint 10", progress: 0, timeline: "May 15 - 26" },
  { title: "Sprint 11", progress: 0, timeline: "May 29 - Jun 2" },
  { title: "Sprint 12", progress: 0, timeline: "Jun 5 - 16" },
];

const dataTask = {
  sprint: "Sprint 7",
  task: [
    {
      date: "Apr 3",
      listTask: [
        {
          project: "Moxa",
          todo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
          team: [images.PERSON4, images.PERSON8],
        },
        {
          project: "Moxa",
          todo: "Fames ac turpis egestas integer eget aliquet nibh praesent tristique. Ultricies mi eget mauris pharetra et ultrices neque ornare aenean.",
          team: [images.PERSON4, images.PERSON2],
        },
        {
          project: "Berijalan",
          todo: "Pulvinar mattis nunc sed blandit libero. Massa sed elementum tempus egestas sed sed risus pretium quam. Quisque egestas diam in arcu cursus euismod.",
          team: [images.PERSON5, images.PERSON2, images.PERSON7],
        },
      ],
    },
    {
      date: "Apr 4",
      listTask: [],
    },
    {
      date: "Apr 5",
      listTask: [
        {
          project: "Setir Kanan",
          todo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
          team: [images.PERSON10, images.PERSON3],
        },
        {
          project: "Moxa",
          todo: "Fames ac turpis egestas integer eget aliquet nibh praesent tristique. Ultricies mi eget mauris pharetra et ultrices neque ornare aenean.",
          team: [images.PERSON2],
        },
      ],
    },
  ],
};

export { dataSprint, dataTask };
