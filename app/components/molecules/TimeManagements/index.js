import Text from "@atoms/Text";
import React, { useEffect, useState } from "react";
import { View } from "react-native";
import Sprint from "./Sprint";
import Progress from "./Progress";
import { dataSprint } from "./data";
import Task from "./Task";

const TimeManagements = () => {
  const [selected, setSelected] = useState(6);
  const [status, setStatus] = useState("Done");

  useEffect(() => {
    if (dataSprint[selected].progress === 0) {
      setStatus("Not Started");
    } else if (dataSprint[selected].progress === 1) {
      setStatus("Done");
    } else {
      setStatus("In Progress");
    }
  }, [selected]);

  const scroll = (val) => {
    if (selected < dataSprint.length - 1 && val === "plus") {
      setSelected(selected + 1);
    }

    if (selected > 0 && val === "minus") {
      setSelected(selected - 1);
    }
  };
  return (
    <View style={{ marginTop: 26 }}>
      <Text bold fontSize={30}>
        Time Management
      </Text>
      <Sprint selected={selected} />
      <Progress
        status={status}
        selected={selected}
        plus={() => scroll("plus")}
        minus={() => scroll("minus")}
      />
      <Task />
    </View>
  );
};

export default TimeManagements;
