import React from "react";
import { Text as TextView } from "react-native";
import { Colors, Fonts, moderateScale } from "@styles/index";

const Text = ({
  children,
  style,
  maxLine,
  lineHeight,
  color = Colors.TextColor.Teks,
  fontSize = 14,
  regular = true,
  black = false,
  blackItalic = false,
  bold = false,
  boldItalic = false,
  extraBold = false,
  extraBoldItalic = false,
  italic = false,
  light = false,
  lightItalic = false,
  semiBold = false,
  semiBoldItalic = false,
  underline = false,
  ...props
}) => (
  <TextView
    {...props}
    allowFontScaling={false}
    numberOfLines={maxLine}
    style={[
      { color: color },
      { fontSize: moderateScale(fontSize) },
      { lineHeight: moderateScale(fontSize * 1.333) },
      regular && { fontFamily: Fonts.Nunito.Regular },
      black && { fontFamily: Fonts.Nunito.Black },
      blackItalic && { fontFamily: Fonts.Nunito.BlackItalic },
      extraBold && { fontFamily: Fonts.Nunito.ExtraBold },
      extraBoldItalic && { fontFamily: Fonts.Nunito.ExtraBoldItalic },
      bold && { fontFamily: Fonts.Nunito.Bold },
      boldItalic && { fontFamily: Fonts.Nunito.BoldItalic },
      italic && { fontFamily: Fonts.Nunito.Italic },
      light && { fontFamily: Fonts.Nunito.Light },
      lightItalic && { fontFamily: Fonts.Nunito.LightItalic },
      semiBold && { fontFamily: Fonts.Nunito.SemiBold },
      semiBoldItalic && { fontFamily: Fonts.Nunito.SemiBoldItalic },
      underline && { textDecorationLine: "underline" },
      style,
    ]}
  >
    {children}
  </TextView>
);

export default Text;
