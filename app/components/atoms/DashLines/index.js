import React from "react";
import { View } from "react-native";

import { Dash } from "@addons/index";
import { Colors } from "@styles/index";

const DashLines = ({
  style,
  color = Colors.WhiteSystem.Fair,
  dash = true,
  width = "100%",
  height = 1,
}) => (
  <View>
    {dash ? (
      <Dash
        dashGap={4}
        dashLength={7}
        dashThickness={1}
        dashColor={color}
        style={[
          {
            height: height,
            width: width,
          },
          style,
        ]}
      />
    ) : (
      <View
        style={[
          {
            height: height,
            width: width,
            backgroundColor: color,
          },
          style,
        ]}
      />
    )}
  </View>
);

export default DashLines;
