import React from "react";
import { View, TouchableNativeFeedback, Platform } from "react-native";
import {
  Colors,
  verticalScale,
  horizontalScale,
  moderateScale,
  Shadow,
  rgbaConverter,
} from "@styles/index";
import { Ripple } from "@addons/index";

const padding = {
  paddingHorizontal: horizontalScale(16),
  paddingVertical: horizontalScale(16),
};

const Card = ({
  children,
  backgroundColor = Colors.White,
  colorRipple = Colors.Soft,
  style,
  styleCard,
  havePadding = true,
  withShadow = true,
  onPress,
  ...props
}) => (
  <View
    style={[
      {
        // borderWidth: 1,
        borderRadius: 12,
        backgroundColor: Colors.Background,
      },
      withShadow && Shadow,
      style,
    ]}
    {...props}
  >
    <Ripple
      onPress={() => onPress || console.log("PRESS")}
      rippleColor={rgbaConverter(Colors.Black, 0.7)}
      rippleDuration={500}
      rippleSize={600}
      style={[
        { flex: 1, borderRadius: 12, overflow: "hidden" },
        havePadding && { padding: horizontalScale(16) },
        styleCard,
      ]}
    >
      {children}
    </Ripple>
  </View>
);

export default Card;
