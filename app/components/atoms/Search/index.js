import React from "react";
import { View, TextInput } from "react-native";

import { Fonts, Colors, horizontalScale } from "@styles/index";
import { SearchIcon } from "@images/index";

const Search = ({ fonStyle = "Regular", fonstSize = 16, placeholder }) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.WhiteSystem.Massive,
        paddingVertical: 15,
        paddingHorizontal: 20,
        borderRadius: 90,
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <View style={{ marginRight: 10 }}>
        <SearchIcon
          width={horizontalScale(17)}
          height={horizontalScale(17)}
          stroke={Colors.TextColor.Teks50}
        />
      </View>
      <TextInput
        style={{
          flex: 1,
          fontFamily: Fonts.Nunito[fonStyle],
          fontSize: fonstSize,
          padding: 0,
        }}
        placeholder={placeholder}
        placeholderTextColor={Colors.TextColor.Teks50}
      />
    </View>
  );
};

export default Search;
