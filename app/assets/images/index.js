export default {
  BACKGROUND: require("./img/bgscreen.png"),
  LOGO: require("./img/logo.png"),

  IC_CANVAS: require("./icon/addLeads.png"),

  CARD_BERIJALAN: require("./img/cardBerijalan.png"),
  CARD_SETIRKANAN: require("./img/cardSetirKananan.png"),
  CARD_MOXA: require("./img/cardMoxa.png"),

  PERSON1: require("./img/person/person1.png"),
  PERSON2: require("./img/person/person2.png"),
  PERSON3: require("./img/person/person3.png"),
  PERSON4: require("./img/person/person4.png"),
  PERSON5: require("./img/person/person5.png"),
  PERSON6: require("./img/person/person6.png"),
  PERSON7: require("./img/person/person7.png"),
  PERSON8: require("./img/person/person8.png"),
  PERSON9: require("./img/person/person9.png"),
  PERSON10: require("./img/person/person10.png"),

  BG_PROFILE: require("./img/bgProfile.png"),

  IC_BERIJALAN: require("./icon/berijalan.png"),
  IC_SETIRKANAN: require("./icon/setirkanan.png"),
  IC_MOXA: require("./icon/moxa.png"),
};

import HomeIcon from "./svg/home.svg";
import LeadsIcon from "./svg/leads.svg";
import EditIcon from "./svg/editing.svg";
import ProfileIcon from "./svg/person.svg";
import PerformIcon from "./svg/perform.svg";
import SearchIcon from "./svg/search.svg";
import TaskIcon from "./svg/task.svg";
import RowRight from "./svg/rowRight.svg";
import ChevronRight from "./svg/chevronRight.svg";
import ChevronLeft from "./svg/chevronLeft.svg";
import SecurityIcon from "./svg/security.svg";
import HelpIcon from "./svg/help.svg";
import AboutIcon from "./svg/about.svg";
import LogoutIcon from "./svg/logout.svg";
import ChevronDown from "./svg/chevronDown.svg";
import ChevronUp from "./svg/chevronUp.svg";

export {
  HomeIcon,
  LeadsIcon,
  EditIcon,
  ProfileIcon,
  PerformIcon,
  SearchIcon,
  TaskIcon,
  RowRight,
  ChevronRight,
  ChevronLeft,
  SecurityIcon,
  HelpIcon,
  AboutIcon,
  LogoutIcon,
  ChevronDown,
  ChevronUp,
};
