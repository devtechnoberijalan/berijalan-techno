import { Dimensions, StatusBar } from "react-native";

const Fonts = {
  Nunito: {
    Black: "Nunito-Black",
    BlackItalic: "Nunito-BlackItalic",
    Bold: "Nunito-Bold",
    BoldItalic: "Nunito-BoldItalic",
    ExtraBold: "Nunito-ExtraBold",
    ExtraBoldItalic: "Nunito-ExtraBoldItalic",
    Italic: "Nunito-Italic",
    Light: "Nunito-Light",
    LightItalic: "Nunito-LightItalic",
    Regular: "Nunito-Regular",
    SemiBold: "Nunito-SemiBold",
    SemiBoldItalic: "Nunito-SemiBoldItalic",
  },
};

const Colors = {
  TreePoppy: {
    Massive: "#945612",
    Heavy: "#C57218",
    Fair: "#F68F1E",
    Soft: "#F8A54B",
    Root: "#FBD2A5",
  },
  Endeavour: {
    Massive: "#003264",
    Heavy: "#004385",
    Fair: "#0054A6",
    Soft: "#3376B8",
    Root: "#6698CA",
  },
  WhiteSystem: {
    Massive: "#FBFBFB",
    Heavy: "#f5f3f6",
    Fair: "#E7E7E7",
    Soft: "#D3D3D3",
    Root: "#A7A7A7",
  },
  RedSystem: {
    Massive: "#C16262",
    Heavy: "#F47C7C",
    Fair: "#EF9F9F",
    Soft: "#FAD4D4",
    Root: "#FFF2F2",
  },
  BlackSystem: {
    Massive: "#222222",
    Heavy: "#383838",
    Fair: "#335661",
    Soft: "#7A7A7A",
    Root: "#909090",
  },
  Malibu: {
    Massive: "#205899",
    Heavy: "#2A75CC",
    Fair: "#3592FF",
    Soft: "#e6e2ff",
    Root: "#f8f3ff",
  },
  TextColor: {
    Teks: "#160520",
    Teks100: "#222222",
    Teks90: "#383838",
    Teks80: "#424242",
    Teks72: "#707E8D",
    Teks70: "#7A7A7A",
    Teks65: "#7E7E7E",
    Teks62: "#9C9C9C",
    Teks60: "#909090",
    Teks50: "#A7A7A7",
    Teks40: "#BDBDBD",
    Teks30: "#D3D3D3",
    Teks20: "#E9E9E9",
    Teks10: "#FBFBFB",
  },
  Error: {
    Massive: "#640000",
    Heavy: "#850000",
    Fair: "#A60000",
    Soft: "#B83333",
    Root: "#CA6666",
  },
  Success: {
    Hard: "#539165",
    Massive: "#AACA66",
    Heavy: "#A4BC92",
    Fair: "#B3C99C",
    Soft: "#C7E9B0",
    Root: "#DDFFBB",
  },
  Info: {
    Massive: "#205899",
    Heavy: "#2A75CC",
    Fair: "#3592FF",
    Soft: "#5DA8FF",
    Root: "#B5D7FF",
  },
  Warning: {
    Massive: "#C89C02",
    Heavy: "#E1AF02",
    Fair: "#FAC302",
    Soft: "#FCD54E",
    Root: "#FDE180",
  },
  Grey: {
    Massive: "#7E7E7E",
    Heavy: "#A4A4A4",
    Fair: "#CED1D4",
    Soft: "#F9F9F9",
    Root: "#F0F0F0",
  },
  Primary: "#04325F",
  ActivePrimary: "#2C598D",
  Background: "#FFFFFF",
  White: "#FFFFFF",
  Black: "#000000",
  SmoothGrey: "#F0F5FA",
  Form: "#FFFFFF",
  FormBorder: "#D6D6D6",
  FormError: "#f9E9E9",
  Google: "#F4EFEF",
  WhiteTransparent: "rgba(255, 255, 255, 0.5)",
  BlackTransparent: "rgba(0, 0, 0, 0.8)",
  ModalTransparent: "rgba(0, 0, 0, 0.5)",
  Transparent: "rgba(0, 0, 0, 0)",
  WhiteTranscluent: "rgba(255, 255, 255, 0.75)",
  BlackTranscluent: "rgba(0, 0, 0, 0.75)",
  TitleText: "#424242",
  BorderTextField: "#D6D6D6",
  RedAlert: "#AC2020",
  UnActife: "#EDEDED",
  Random: () =>
    "rgb(" +
    Math.floor(Math.random() * 256) +
    "," +
    Math.floor(Math.random() * 256) +
    "," +
    Math.floor(Math.random() * 256) +
    ")",
};

const rgbaConverter = (hexCode, opacity = 1) => {
  let hex = hexCode.replace("#", "");

  if (hex.length === 3) {
    hex = `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}`;
  }

  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);

  /* Backward compatibility for whole number based opacity values. */
  if (opacity > 1 && opacity <= 100) {
    opacity = opacity / 100;
  }

  return `rgba(${r},${g},${b},${opacity})`;
};

//Responsive UI
const Width = Dimensions.get("screen").width;
const Height = Dimensions.get("screen").height;
const HeightWindow = Dimensions.get("window").height;

const STATUS_BAR_HEIGHT = StatusBar.currentHeight;
const HeightNav = Height - (HeightWindow + STATUS_BAR_HEIGHT);

const guidelineBaseWidth = 375;
const guidelineBaseHeight = 812;

const horizontalScale = (size) => (Width / guidelineBaseWidth) * size;
const verticalScale = (size) => (Height / guidelineBaseHeight) * size;
const moderateScale = (size, factor = 0.5) =>
  size + (horizontalScale(size) - size) * factor;

const Shadow = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 3.84,

  elevation: 5,
};

export {
  Fonts,
  Colors,
  Width,
  Height,
  Shadow,
  HeightWindow,
  HeightNav,
  STATUS_BAR_HEIGHT,
  horizontalScale,
  verticalScale,
  moderateScale,
  rgbaConverter,
};
