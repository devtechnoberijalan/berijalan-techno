import uuid from "react-native-uuid";

const key = (value) => {
  const uniq = "f0b8a8ae-0f04-5215-89ec-5da58f6ff228";
  return uuid.v5(value.toString(), uniq);
};

export { key };
