import { Colors, horizontalScale } from "@styles/index";
import React, { Component, useCallback, useState } from "react";
import { Image, Platform, Pressable, StyleSheet, View } from "react-native";

import { Container } from "@templates/index";
import { Text } from "@atoms/index";
import images, {
  HomeIcon,
  LeadsIcon,
  PerformIcon,
  ProfileIcon,
} from "@images/index";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "../Home";
import Leads from "../Leads";
import Canvassing from "../Canvassing";
import Performa from "../Performa";
import Profile from "../Profile";

const Tab = createBottomTabNavigator();

function Main({ navigation }) {
  const [initialRouteName, setInitialRouteName] = useState("Beranda");
  const [routeIndex, setRouteIndex] = useState(0);

  const tabBarLabel = (focused, color, route) => (
    <Text
      bold={focused}
      fontSize={10}
      color={focused ? Colors.TextColor.Teks : Colors.TextColor.Teks72}
    >
      {route.name !== "Canvas" && route.name}
    </Text>
  );

  const barIcons = (focused, color, size, route) => {
    let tabBarIcon = {
      Home: (
        <HomeIcon
          width={horizontalScale(size)}
          height={horizontalScale(size)}
          fill={color}
        />
      ),
      Task: (
        <LeadsIcon
          width={horizontalScale(size)}
          height={horizontalScale(size)}
          fill={color}
        />
      ),
      Canvas: (
        <Image
          style={{
            width: horizontalScale(size * 3.6),
            height: horizontalScale(size * 3.6),
            marginBottom: horizontalScale(20),
          }}
          resizeMode={"contain"}
          source={images.IC_CANVAS}
        />
      ),
      Performance: (
        <PerformIcon
          width={horizontalScale(size)}
          height={horizontalScale(size)}
          fill={color}
        />
      ),
      Profile: (
        <ProfileIcon
          width={horizontalScale(size)}
          height={horizontalScale(size)}
          fill={color}
        />
      ),
    };

    return <View>{tabBarIcon[route.name]}</View>;
  };

  return (
    <Container>
      <Tab.Navigator
        initialRouteName={initialRouteName}
        screenListeners={({ route }) => ({
          state: (e) => {
            // Do something with the state
            // console.log('state changed', e.data.state.index)
            setRouteIndex(e.data.state.index);
            // this.loadIP()
            // console.log('route', route)
          },
        })}
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarHideOnKeyboard: true,
          tabBarActiveTintColor: Colors.Primary,
          tabBarInactiveTintColor: Colors.Grey.Fair,
          tabBarLabel: ({ focused, color }) =>
            tabBarLabel(focused, color, route),
          tabBarIcon: ({ focused, color, size }) =>
            barIcons(focused, color, size, route),
          tabBarStyle: {
            paddingBottom: horizontalScale(Platform.OS === "ios" ? 25 : 10),
            paddingTop: horizontalScale(10),
            height: horizontalScale(Platform.OS === "ios" ? 85 : 65),
          },
        })}
      >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Task" component={Leads} />
        <Tab.Screen
          name="Canvas"
          component={Canvassing}
          listeners={{
            tabPress: (e) => {
              // Prevent default action
              e.preventDefault();

              //Any custom code here
              navigation.navigate("Canvassing");
            },
          }}
        />
        <Tab.Screen name="Performance" component={Performa} />
        <Tab.Screen name="Profile" component={Profile} />
      </Tab.Navigator>
    </Container>
  );
}
export default Main;
