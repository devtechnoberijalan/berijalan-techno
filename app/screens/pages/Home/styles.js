import { StyleSheet } from "react-native";
import {
  Colors,
  horizontalScale,
  moderateScale,
  verticalScale,
} from "@styles/index";

export default StyleSheet.create({
  container: {
    marginTop: verticalScale(45),
  },

  header: {
    // backgroundColor: "#48dbfb",
    flexDirection: "row",
    marginBottom: 20,
  },

  iconTask: {
    backgroundColor: Colors.WhiteSystem.Massive,
    padding: 20,
    borderRadius: moderateScale(90),
    marginRight: 16,
  },
});
