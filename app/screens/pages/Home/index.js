import React from "react";
import { View } from "react-native";

import { Search, Text } from "@atoms/index";
import styles from "./styles";
import { Container, Content } from "@templates/index";
import { TouchableOpacity } from "react-native";
import { TaskIcon } from "@images/index";
import { Colors, horizontalScale, moderateScale } from "@styles/index";
import { Projects, TimeManagements } from "@molecules/index";

function Home({ navigation, route }) {
  return (
    <Container>
      <Content style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity style={styles.iconTask}>
            <TaskIcon
              width={horizontalScale(20)}
              height={horizontalScale(20)}
              fill={Colors.TextColor.Teks50}
            />
          </TouchableOpacity>
          <Search fonStyle={"Bold"} placeholder={"Find Your Project"} />
        </View>
        <Projects />
        <TimeManagements />
      </Content>
    </Container>
  );
}
export default Home;
