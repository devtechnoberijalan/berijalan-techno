import React from "react";
import {
  Image,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import Constants from "expo-constants";

import { Text } from "@atoms/index";
import { Container, Content } from "@templates/index";
import {
  Colors,
  Height,
  Shadow,
  Width,
  horizontalScale,
  verticalScale,
} from "@styles/index";
import {
  ImageProfile,
  PersonalInfo,
  TeamProject,
  AppInfo,
} from "@molecules/index";

import styles from "./styles";

function Profile({ navigation, route }) {
  const version = Constants.manifest.version;
  return (
    <Container>
      <Content padding={false}>
        <ImageProfile />
        <View style={styles.content}>
          <PersonalInfo />
          <TeamProject />
          <AppInfo />
        </View>
        <Text bold style={styles.appVersion}>
          v{version}
        </Text>
      </Content>
    </Container>
  );
}
export default Profile;
