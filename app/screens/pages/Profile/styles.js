import { StyleSheet } from "react-native";
import {
  Colors,
  horizontalScale,
  moderateScale,
  verticalScale,
} from "@styles/index";

export default StyleSheet.create({
  content: { marginTop: verticalScale(-210), paddingHorizontal: 16 },

  appVersion: {
    marginTop: 20,
    textAlign: "center",
  },
});
