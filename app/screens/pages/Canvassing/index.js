import React from 'react'
import { StyleSheet, View } from 'react-native'

import { Text } from '@atoms/index'

function Canvassing({ navigation, route }) {
  return (
    <View style={styles.container}>
      <Text>CanvassingScreen</Text>
    </View>
  )
}
export default Canvassing

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
