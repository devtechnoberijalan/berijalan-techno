import React from 'react'
import { StyleSheet, View } from 'react-native'

import { Text } from '@atoms/index'

function Leads({ navigation, route }) {
  return (
    <View style={styles.container}>
      <Text>LeadsScreen</Text>
    </View>
  )
}
export default Leads

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
