import React from "react";
import { StyleSheet, View } from "react-native";

import { Text } from "@atoms/index";

function Performa({ navigation, route }) {
  return (
    <View style={styles.container}>
      <Text>PerformaScreen</Text>
    </View>
  );
}
export default Performa;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
