import React, { useState, useEffect, useCallback } from "react";
import { View } from "react-native";

import * as SplashScreen from "expo-splash-screen";
import * as Font from "expo-font";

import { Splash } from "@templates/index";
import fonts from "@fonts/index";

console.ignoredYellowBox = ["Warning: Failed propType: SceneView"];

const PersistGate = ({ children, persistor }) => {
  const [rehydrated, setRehydrated] = useState(false);
  const [initiated, setInitiated] = useState(true);

  useEffect(() => {
    async function prepare() {
      try {
        await Font.loadAsync(fonts);
      } catch (e) {
        console.warn(e);
      } finally {
        // Tell the application to render
        rehydrate();
      }
    }

    prepare();
  }, []);

  const rehydrate = async () => {
    setRehydrated(true);

    await SplashScreen.hideAsync();
  };

  return (
    <View style={{ flex: 1 }}>
      {rehydrated && initiated ? children : <Splash />}
    </View>
  );
};

export default PersistGate;
