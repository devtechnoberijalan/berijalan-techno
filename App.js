import { Fragment } from "react";
import { StatusBar } from "expo-status-bar";
import { StyleSheet, View } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Provider } from "react-redux";

import { PersistGate } from "@middleware/index";
import { store, persistor } from "@storage/index";
import IndexLayout from "@router/index";
const AppLayout = IndexLayout;

export default function App() {
  return (
    <Fragment>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <SafeAreaProvider>
            <AppLayout />
          </SafeAreaProvider>
        </PersistGate>
      </Provider>
    </Fragment>
  );
}
